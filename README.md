# filevers
filevers is a program that allow to keep track of all the changes done to all  
the files in the selected folders  

The program continuously scan the folders and checks for changed files  
each time a changed file is found a new copy of it is stored internally  

filevers run on both linux and windows though currently there are some bugs on  
windows that are a bit annoying

# Discontinued
I've tested the program, compiled and tested it under windows and linux and even if there 
are small problems it works, some instructions on how to compile it and explenation on the 
code are probably needed but i'm not following the project anymore and so i'm relasing it under public domain

# Usage  
To use the tool donwload the binary from the Releases section and run it  
if you are on linux you will probably need to enable the file to be executed

### Start a scan
the basic usage of the tool consist on setting all the folder to be scanned  
and then start the scan  
in order to start a scan for all the configured folders it's enough to run this command  
`filevers -start`  
the program then will start and a line will be printed on the console:  
`press any key to stop`  
this means that the program is correcly working, to stop the scan just press  
any key to the console, or just close it

### Configure the folders
to configure the folders to scan use  
`filvers -set scanFolder "/example"`  
this will add a new entry to the configuration file specifing that the "example"  
folder that is present at the root should be scan  
under windows the same command will look like this  
`filevers -set scanFolder "C:\\example"`

each folder should be specified with its absolute path  

To show all the current settings use:  
`filevers -settings`  
and to remove a folder in the setting use the `remset` option, in the same way of the `set`:
`filevers -remset scanFolder "/example"`  
it's not possible to remove a setting without specify it's value

### Start a scan without use the settings
If you don't want to add a new folder to the setting file it's possible to  
start a scan specifying directly the folder to scan:  
`filvers -scan "/example"`

### Check the version of a file
when you want to check for an old version of a file you can use the the `vers` command  
this shows all the versions of the file with the time when the change was done:  
`filevers -vers "/example/file.txt"`  
this will show all the versions of the `file.txt` file in the `example` folder  

in order to see the content of a specific version use the previous command  
specifying the version of the file that you want to see:  
`filevers -vers "/example/file.txt" 1`  

if you want to check all the files that has been versioned you can use:  
`filevers -files`  
and a list of files with the path will be shown  

# Graphical version
A graphical version already exists for filevers, but I want to clean the code and  
solve some bugs before to release it

# Installers
There are actually no installers for this program  
The plan is to create a deb package that will install the service to run filevers at  
the start of the computer and then create a separate one for the GUI  
For windows I've already an installer that installs the program with the GUI and  
set the scan to run at the start of the computer, but I will wait to sort the  
GUI problem and it's release before to make it available


# Problems
If you have some problems using the tool or you have some question about the program    
please create a new Issue in the Issues section explaining the problem  
If you don't have a GitLab account or you just prefer to talk directly with me  
you can send an email to sambccd@gmail.com

# Contribute
The easier way to contribute is cloning the [filevers-compile](https://gitlab.com/sambuccid/filevers-compile)  
project, in the project there are all the needed compilers along  
a script `Compile.sh` that compiles the program for linux with all the unittests enabled  
To compile the program in windows the dlang compiler it's needed but it's not currently in the 
filevers-compile project, although the program works on windows i haven't provided the process to compile it
it should not be hard to do, it's alsmost the same as the windows one, but i'm not spending much time on this 
project anymore

