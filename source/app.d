import std.stdio;
import std.conv;
import std.string: isNumeric;
import Util;
import Settings;
import Control;
import Settings: getSettings,getSettingByKey,addSetting,Setting,SettingParseExc;
import Scanning;
import Logging:Log;

void main(string[] args){
    if(minArgsNum(args,2)) return;
    setup();
    refreshSettings();
    string[] par=args[2..$];
    string com=args[1];
    if(args[1]=="-debug"){
        log=Log.newLog(true,true);
        par=par[1..$];
        com=args[2];
    }
    switch(com){
    case "-start":startCom();
        break;
    case "-service": serviceCom();
        break;
    case "-files":filesCom();
        break;
    case "-settings":settingsCom();
        break;
    case "-settingslist":settingsCom(false);
        break;
    case "-set":
        if(minArgsNum(par,2)) return;
        setCom(par[0],par[1]);
        break;
    case "-remset":
        if(minArgsNum(par,2)) return;
        remsetCom(par[0],par[1]);
        break;
    case "-vers":
        if(minArgsNum(par,1)) return;
        if(par.length<=1)
            versCom(par[0]);
        else{
            if(!isNumeric(par[1])){
                writeln("the second parameter should be a number");
                return;
            }
            selectedVersCom(par[0],to!int(par[1]));//think about that maybe another command it's better for this instead of use the same one "vers"
        }
        break;
    case "-scan":
        if(minArgsNum(par,1)) return;
        scanCom(par[0]);
        break;
    case "-checkval":
        if(minArgsNum(par,2)) return;
        checkvalCom(par[0],par[1]);
        break;
    default:showHelp();
        break;
    }
}

private void startCom(){
    Util.start();
    writeln("press a key to stop");
    readln();
    Util.stop();
}

private void serviceCom(){
    Util.start();
}

private void filesCom(){
    string[] files=getAllVersionedFiles();
    foreach(file;files){
        writeln(file);
    }
}

private void settingsCom(bool pretty=true){
    Setting[Setting.Key] settings=getSettings();

    foreach(key,setting;settings){
        writeln(key~":");
        foreach(val;setting.getValueList()){
            writeln((pretty?"  ":"")~val);
        }
    }
}

private void setCom(string prop,string value){
    if(!Setting.isValidKey(prop)){
        writeln("property not valid");
        return;
    }
    try{
        addSetting(Setting.getKey(prop),value);
    }catch(SettingParseExc e){
        switch(e.errType){
        case SettingParseExc.ErrType.InvalidPropValue:
            writeln("the inserted value is not valid");
            break;
        case SettingParseExc.ErrType.UnexpectedMultipleValues:
            //writeln("the inserted value can't have multiple values");
            Setting i=getSettingByKey(Setting.getKey(prop));
            deleteSetting(i.getKey(prop),i.getValueList[0]);
            setCom(prop,value);
            //writeln("old value substituted");
            break;
        default:
            writeln("the inserted setting it's not valid");
        }
    }
}

private void remsetCom(string prop, string val){
    if(!Setting.isValidKey(prop)){
        writeln("property not valid");
        return;
    }
    if(!deleteSetting(Setting.getKey(prop),val)){
        writeln("Setting not found");
        return;
    }
}

private void versCom(string file){
    Change[] vers;
    try{
        vers=getFileVersions(file);
    }catch(FolderNotExistsExc e){
        writeln("the specified folder it's not been versioned");
        return;
    }

    foreach(i, ver;vers){
        writeln(to!string(i+1)~" "~ver.timestamp);
    }
}

private void selectedVersCom(string file, uint number){
    Change[] vers=getFileVersions(file);

    if(number<1){
        writeln("the count of the version start from 1");
        return;
    }
    number--;//(start to count from 0)
    if(vers.length<=number){
        writeln("there are just "~to!string(vers.length)~" versions of the file");
        return;
    }
    writeln(vers[number].text);
}

private void scanCom(string dir){
    Scanning.start(dir,30);
    writeln("press a key to stop");
    readln();
    Scanning.stop();
}

private void checkvalCom(string prop, string val){
    if(!Setting.isValidKey(prop)){
        writeln("N: property not valid");
        return;
    }
    if(!Setting.isValidValue(Setting.getKey(prop),val)){
        writeln("N: value not valid");
        return;
    }
    writeln("Y: property and value are valid");
}

private void showHelp(){
    writeln("option not recognized
the program accept the following parameters:
     -start: to start the scan of all the folders listed in your configuration file
     -files: to get the name of all the versione files
     -vers path/file.ext: to get a list of all the version of a specified file
     -vers path/file.ext 1: to get a version of the specified file
     -scan path: to start a scan of a specific folder manually
     -settings: to list the current settings
     -set property value: to set the value of a setting, valid settings are scanFolder and scanFrequency
     -remset property value: remove an existing setting");
}

private bool minArgsNum(string[] args, uint expectedNum){
    if(args.length<expectedNum){
        writeln("not enought arguments passed, please refers to the help");
        return true;
    }
    return false;
}


