module Differences;
import std.string: lineSplitter;
import std.array;
import std.conv: to;
import std.range:iota;
import Util;

LineDifference[] linesDifferences(string string1,string string2){
    log.call("lineDifferences", string1.replace("\n","\\n")~" - "~string2.replace("\n","\\n"));
    string[] linesOf1=lineSplitter(string1).array;
    string[] linesOf2=lineSplitter(string2).array;
    LineDifference[] differences;
    uint idx1=0;
    uint idx2=0;
    while(linesOf1.length>idx1 || linesOf2.length>idx2){
        if(linesOf1.length<=idx1){
            log.alg("lines1 ended, added all the remaining lines2");
            differences~=blockOfLines2(idx1,idx2,linesOf2.length);
            idx2=to!uint(linesOf2.length);
            continue;
        }else if(linesOf2.length<=idx2){
            log.alg("lines2 ended, added all the remaining lines1");
            differences~=blockOfLines1(idx1,linesOf1.length,idx2);
            idx1=to!uint(linesOf1.length);
            continue;
        }
        string line1=linesOf1[idx1];
        string line2=linesOf2[idx2];
        log.alg("idxs: "~to!string(idx1)~" "~to!string(idx2));
        if(line1!=line2){
            if(areSimilar(line1,line2)){
                log.alg("lines similar");
                LineDifference ld = {lineFile1:idx1, modifier: '~', lineFile2:idx2};
                differences ~= ld;
                idx1++;idx2++;
            }else{
                log.alg("lines different");
                uint[] newIdxs=findEndBlock(linesOf1,linesOf2,idx1,idx2);
                differences~=blockOfLines1(idx1,newIdxs[0],idx2);
                differences~=blockOfLines2(idx1,idx2,newIdxs[1]);
                idx1=newIdxs[0];
                idx2=newIdxs[1];
            }
        }else{
            idx1++;idx2++;
        }
    }
    
    log.ret(differences);
    return differences;
}

//gived 2 list of string and their indexes it find the next lines where both the lists have a similar string
//returns always a list with 2 integers representing the 2 new indexes
private uint[] findEndBlock(string[] lines1,string[] lines2,uint idx1,uint idx2)
out(res){
    assert(res.length==2);
}body{
    log.call("findEndBlock",to!string(idx1)~"  "~to!string(idx2));
    string[] tmp1;
    string[] tmp2;
    uint newIdx1=idx1;
    uint newIdx2=idx2;
    while(to!uint(lines1.length)>newIdx1 || to!uint(lines2.length)>newIdx2){
        log.alg("idxs:"~to!string(newIdx1)~"  "~to!string(newIdx2));
        if(lines1.length<=newIdx1){
            log.alg("lines1 ended, returning all lines2");
            newIdx1=to!uint(lines1.length)-1;
            newIdx2=to!uint(lines2.length)-1;
            continue;
        }else if(lines2.length<=newIdx2){
            log.alg("lines2 ended, returning all lines1");
            newIdx1=to!uint(lines1.length)-1;
            newIdx2=to!uint(lines2.length)-1;
            continue;
        }
        if(areSimilar(lines1[newIdx1],lines2[newIdx2])){
            break;
        }else{
            if(matchInList(tmp2,lines1[newIdx1])!=-1){
                log.alg("matched in list2");
                uint idxInTmp2=matchInList(tmp2,lines1[newIdx1]);
                newIdx2=idx2+idxInTmp2;
                break;
            }else if(matchInList(tmp1,lines2[newIdx2])!=-1){
                log.alg("matched in list1");
                uint idxInTmp1=matchInList(tmp1,lines2[newIdx2]);
                newIdx1=idx1+idxInTmp1;
                break;
            }
            log.alg("not matched, added lines in lists");
            tmp1~=lines1[newIdx1];
            tmp2~=lines2[newIdx2];
            newIdx1++;newIdx2++;
        }
    }
    uint[] ret;
    ret~=newIdx1;
    ret~=newIdx2;
    log.ret(ret);
    return ret;
}

//search for a similar line in the list, return the index of the line or -1 if there isn't
private uint matchInList(string[] list,string line){
    foreach(i, lineInList;list){
        if(areSimilar(lineInList,line)){
            return to!uint(i);
        }
    }
    return -1;
}

//the 2 lines starts with the same letter, for now means that they are similar
private bool areSimilar(string s1,string s2){
    return s1[0]==s2[0];
}

private LineDifference[] blockOfLines1(ulong idx1Start,ulong idx1End,ulong idx2){
    ulong[] blockOfLines1=iota(idx1Start,idx1End).array();
    return generateDifference(blockOfLines1,'>',idx2);//the 2 functions now can be grouped in just one
}

private LineDifference[] blockOfLines2(ulong idx1,ulong idx2Start,ulong idx2End){
    ulong[] blockOfLines2=iota(idx2Start,idx2End).array;
    return generateDifference(idx1,'<',blockOfLines2);
}

private LineDifference[] generateDifference(ulong[] idxs1,char modifier,ulong idx2){
    LineDifference[] list;
    foreach(idx1;idxs1){
        LineDifference ld={idx1,modifier,idx2};
        list~=ld;
    }
    return list;
}

private LineDifference[] generateDifference(ulong idx1,char modifier,ulong[] idxs2){
    LineDifference[] list;
    foreach(idx2;idxs2){
        LineDifference ld={idx1,modifier,idx2};
        list~=ld;
    }
    return list;
}

struct LineDifference{
    ulong lineFile1;
    char modifier;//can be <, > and ~
    ulong lineFile2;
}

string stringify(LineDifference ld){
    return to!string(ld.lineFile1) ~ ld.modifier ~ to!string(ld.lineFile2);
}
