module Scanning;

import std.path;
import std.file;
import std.stdio: writeln, File, readln;
import std.datetime : SysTime;
import core.thread;
import std.algorithm.sorting: sort;
import std.datetime;
import std.format;
import std.algorithm;
import std.string: indexOf;
import std.concurrency : receive, receiveOnly, send, spawn, thisTid, Tid;
import Util;
import Logging;
import Control;

private static shared(bool) toStop;
void start(string path,long frequency){
    toStop=false;
    spawn(&scanThread,path,frequency,log,thisTid);
    string created = receiveOnly!string();
}
void stop(){
    toStop=true;
}
private bool toBeStoped(){
    return toStop;
}

private void scanThread(string path,long frequency,Log log,Tid parentTid){
    Util.log=log;
    Control c=Control.Control.createNew(path);
    send(parentTid, "created");
    loop(c,frequency);
}

private void loop(Control c, long frequency){
    while(!toBeStoped()){
        c.scan();
        Thread.sleep(frequency.msecs);
    }
}
