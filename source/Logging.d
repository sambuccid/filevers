module Logging;
import std.stdio;

struct Log{
private:
    int tabs=0;
    bool isCallToLog;
    bool isAlgToLog;
    bool isRetToLog;
    this(bool call, bool alg){
        isCallToLog=call;
        isAlgToLog=alg;
    }
public:
    static Log newLog(bool call=false, bool alg=false){
        return Log(call,alg);
    }    
    void call(T)(string methodName,T other=null){
        if(isCallToLog){
            write("D Call, ",addTabs(),methodName);
            if(other!=null) write(" ",other);
            write(" \u2193",'\n');
        }
        tabs++;
    }
    void alg(T)(T other=null){
        if(isAlgToLog){
            write("D alg");
            if(other!=null) write(", ",addTabs(),other);
            write('\n');
        }
    }
    void algTab(T)(T other=null){
        incTabs();
        alg(other);
        decTabs();
    }
    void ret(T)(T other=null){
        if(isCallToLog){
            write("D ret, ",addTabs(),"<");
            if(other !is null) write(other);
            write('\n');
        }
        tabs--;
    }
    private string addTabs(){
        char[] a="".dup;
        foreach(i;0..tabs){a~="    ".dup;}
        return a.idup;
    }
    private void incTabs(){tabs++;}
    private void decTabs(){
        if(tabs<=0)
            throw new Exception("dec tab non possibile");
        tabs--;
    }
}
