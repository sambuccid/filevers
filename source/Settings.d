module Settings;

import std.path;
import std.file;
import std.string;
import std.stdio: File;
import std.exception;
import std.algorithm;
import std.conv: to;
import Util;

class Setting{
    enum Key{SCAN="scanFolder",FREQ="scanFrequency"};
    private Key key;
    private string[] values;

    this(Key key,string[] values){
        log.call("new setting"~key~to!string(values));
        this.key=key;
        this.values=[]~values;
        log.ret();
    }
    this(Key key){
        this(key,[]);
    }

    public static Setting parse(string line,int lineNum){
        settParseEnforce(!line.canFind(delimiter), SettingParseExc.ErrType.NoDelimiter, lineNum, line);
        string keyStr=line[0 .. line.indexOf(delimiter)];
        log.alg("parsed prop:"~keyStr);
        string value=(line[keyStr.length+1 .. $]);
        log.alg("parsed value: "~value);
        SettingParseExc.ErrType err=Setting.validate(keyStr,value);
        if(err!=SettingParseExc.ErrType.NO_ERROR){
            settParseEnforce(true,err,lineNum,line);
        }
        return new Setting(getKey(keyStr),[value]);
    }

    public static SettingParseExc.ErrType validate(string keyStr, string value){
        if(!isValidKey(keyStr))
            return SettingParseExc.ErrType.InvalidPropName;
        Setting.Key theKey=getKey(keyStr);
        if(value.empty)
            return SettingParseExc.ErrType.EmptyValue;
        if(value.indexOf(delimiter)!=-1)
            return SettingParseExc.ErrType.MultiDelimiter;
        if(!isValidValue(theKey,value))
           return SettingParseExc.ErrType.InvalidPropValue;
        return SettingParseExc.ErrType.NO_ERROR;
    }

    public string[] getValueList(){
        log.call("getValueList");
        log.ret(this.values);
        return this.values;
    }

    public void addValue(string value){
        log.call("addValue",value);
        this.values ~= value;
        log.ret(this.values);
    }

    public void merge(Setting other,int lineNum, string line){
        if(this.values.length>0 && other.values.length>0)
            settParseEnforce(!canHaveMultipleValues(this.key), SettingParseExc.ErrType.UnexpectedMultipleValues, lineNum, line);
        this.values~=other.values;
    }
    
    public static Key getKey(string setting){
        enforce(isValidKey(setting));
        if(setting=="scanFolder") return Key.SCAN;
        if(setting=="scanFrequency") return Key.FREQ;
        assert(0);
    }
    public static bool isValidKey(string setting){
        return ["scanFolder","scanFrequency"].canFind(setting);
    }
    public static bool isValidValue(Key setting, string value){
        if(setting==Key.SCAN){
            return exists(value) && isAbsolute(value);
        }
        if(setting==Key.FREQ) return isNumeric(value);
        assert(0);
    }
    public static bool canHaveMultipleValues(Key setting){
        if(setting==Key.SCAN) return true;
        if(setting==Key.FREQ) return false;
        assert(0);
    }
}
//private immutable filePath=programDataFolder~"/versioning.prop";
public string getPathFileSettings(){return buildNormalizedPath(programDataFolder~"/versioning.prop");}
public immutable delimiter='=';
public void atStartup(){
    createFile();
    refreshSettings();
}
private Setting[Setting.Key] settings;
    
public Setting[Setting.Key] getSettings(){
    return settings;
}
public Setting getSettingByKey(Setting.Key key){
    return settings[key];
}

public void refreshSettings(){
    log.call("refreshSettings");
    File file = File(getPathFileSettings, "r");
    settings.clear();
    int lineNum=0;
    while (!file.eof()) {
        string line = strip(file.readln());
        log.alg("reading "~line);
        if(line.length==0)
            continue;
        settParseEnforce(!line.canFind(delimiter), SettingParseExc.ErrType.NoDelimiter, lineNum, line);
        Setting setting=Setting.parse(line,lineNum);
        if(setting.key !in settings){
            log.alg("created new setting");
            settings[setting.key] = setting;
        }else{
            enforce(!settings[setting.key].getValueList().canFind(setting.values[0]));
            settings[setting.key].merge(setting,lineNum,line);
        }
        lineNum++;
    }
    log.ret(settings);
}

public void addSetting(Setting.Key setting, string value){
    log.call("addSetting",setting~":"~value);
    string fileBak=readText(getPathFileSettings());
    try{
        writeSetting(setting,value);
        refreshSettings();
    }catch(SettingParseExc e){
        write(getPathFileSettings(),fileBak);
        throw e;
    }
    log.ret();
}

public bool deleteSetting(Setting.Key setting, string value){
    if(setting !in settings || !settings[setting].getValueList().canFind(value))
        return false;
    immutable ulong nSettings=settings[setting].getValueList().length;
    
    File fileSettings=File(getPathFileSettings,"r");
    File modifiedFile=File(getPathFileSettings~"1","w");
    while (!fileSettings.eof()) {
        string line = strip(fileSettings.readln());
        if(line!=formatSetting(setting,value)){
            modifiedFile.writeln(line);
        }
    }
    fileSettings.close();
    modifiedFile.close();
    remove(getPathFileSettings);
    rename(getPathFileSettings~"1",getPathFileSettings);
    refreshSettings();

    if(nSettings==1)
        assert(setting !in settings);
    else
        assert(settings[setting].getValueList().length==nSettings-1);
    return true;
}

private void writeSetting(Setting.Key setting, string value){
    log.call("writeSetting",setting~":"~value);
    if(setting !in settings || !settings[setting].getValueList().canFind(value))
        getPathFileSettings.append(formatSetting(setting,value) ~"\n");
    log.ret();
}

private string formatSetting(Setting.Key setting, string value){
    return setting~ delimiter ~value;
}

private void createFile(){
    if(!fileIsPresent()){
        write(getPathFileSettings,"");
    }
}

private bool fileIsPresent(){
    return exists(getPathFileSettings);
}

private void settParseEnforce(bool condition, SettingParseExc.ErrType errType, int lineNum, string line){
    log.call("settParseEnforce");
    if(condition){
        log.alg("throwing parse exception of type:"~to!string(errType));
        switch(errType){
        case SettingParseExc.ErrType.MultiDelimiter:
            throw new SettingParseExc(errType, lineNum, line, "Found multiple delimiters in the line number:"~to!string(lineNum)~" :"~line);
        case SettingParseExc.ErrType.InvalidPropName:
            throw new SettingParseExc(errType, lineNum, line, "Property not recognised in the line number:"~to!string(lineNum)~" :"~line);
        case SettingParseExc.ErrType.NoDelimiter:
            throw new SettingParseExc(errType, lineNum, line, "No delimiter found in the line number:"~to!string(lineNum)~" :"~line);
        case SettingParseExc.ErrType.EmptyValue:
            throw new SettingParseExc(errType, lineNum, line, "Empty value found in the line number:"~to!string(lineNum)~" :"~line);
        case SettingParseExc.ErrType.InvalidPropValue:
            throw new SettingParseExc(errType, lineNum, line, "An invalid value is found in the line number:"~to!string(lineNum)~" :"~line);
        case SettingParseExc.ErrType.UnexpectedMultipleValues:
            throw new SettingParseExc(errType, lineNum, line, "Multiple value found for a property that can have just one value second value foun in line number:"~to!string(lineNum)~" :"~line);
        default: assert(0);
        }
    }
    log.ret();
}

class SettingParseExc:Exception{
    enum ErrType{MultiDelimiter,NoDelimiter,EmptyValue,InvalidPropName,InvalidPropValue,UnexpectedMultipleValues,NO_ERROR};
    const ErrType errType;
    const int lineNumber;
    const string lineStr;
    this(ErrType errType, int lineNum, string lineStr, string msg, string file = __FILE__, size_t line = __LINE__) {
        this.errType=errType;
        this.lineNumber=lineNum;
        this.lineStr=lineStr;
        super(msg, file, line);
    }
    ErrType getErrType(){
        return errType;
    }
}
