module Util;
import std.stdio;
import std.file: mkdir, mkdirRecurse, isDir, exists, rmdirRecurse, remove;
import std.path: dirName, expandTilde, buildNormalizedPath, absolutePath;
import std.algorithm;
import std.array:replace;
import std.conv: to;
import Logging:Log;
import Control;
import Settings;
import Scanning;

private immutable string oldVersions="old";
string dirForTest(){return absolutePath("tests/testDir");}
string otherDirForTest(){return absolutePath("tests/otherTestDir");}
immutable string fsep=",";
public string programDataFolder(){
    import core.stdc.stdlib:getenv;
    if(onWin)
        return buildNormalizedPath((to!string(getenv("APPDATA")))~"/filevers");
    else
        return buildNormalizedPath(expandTilde("~/.fileVers"));
}
public string internalFolder(){return buildNormalizedPath(programDataFolder~"/copiaFiles");}
immutable long defaultScanFreq = 50;
unittest{
    setupTest();
    assert(exists(dirForTest));
    assert(exists(otherDirForTest));
}
void setupTest(bool logging=false){
    emtpyFolder(dirForTest);
    emtpyFolder(otherDirForTest);
    emtpyFolder(getInternalFolder);
    emtpyFolder(getOldVersionsPath);
    if(exists(getPathFileSettings()))
        remove(getPathFileSettings());
    Control.Control.resetControls();
    log=Log.newLog(logging,logging);
    Settings.atStartup();
}

public void setup(){
    if(!exists(programDataFolder)){
        mkdir(programDataFolder);
    }else if(!getInternalFolder.isDir){
        throw new Exception("it's not possible to create the setup folder, as there is already a file with the same name");
    }
    if(!exists(getInternalFolder)){
        mkdir(getInternalFolder);
    }else if(!getInternalFolder.isDir){
        throw new Exception("it's not possible to create the setup folder, as there is already a file with the same name");
    }
    if(!exists(getOldVersionsPath)){
        mkdir(getOldVersionsPath);
    }else if(!(getOldVersionsPath).isDir){
        throw new Exception("it's not possible to create the setup folder, as there is already a file with the same name");
    }
   if(!exists(getPathFileSettings())){
        writeWithDir(getPathFileSettings(),"");
    }else if(getPathFileSettings().isDir){
        throw new Exception("it's not possible to create the config file, as there is already a folder with the same name");
    }
}

public void start(){
    refreshSettings();
    setup();
    Setting[Setting.Key] settings=getSettings();
    if(Setting.Key.SCAN !in settings)
        return;
    long scanFreq = defaultScanFreq;   //BA(be aware) default parameter read not tested
    if(Setting.Key.FREQ in settings && settings[Setting.Key.FREQ].getValueList().length>0){
        string scanFreqStr = settings[Setting.Key.FREQ].getValueList()[0];
        scanFreq = to!long(scanFreqStr);
    }
    foreach(string folder;settings[Setting.Key.SCAN].getValueList()){
        Scanning.start(folder,scanFreq);
    }
}

public void stop(){
    Scanning.stop();
}

void emtpyFolder(string folder){
    //delete old folders
    if(exists(folder)){
        if(folder.isDir){
            folder.rmdirRecurse();
        }else{
            folder.remove();
        }
    }
    //create new directory
    mkdirRecurse(folder);
}

string addOldVers(string path){
    if(path.length<=0)
        return buildNormalizedPath(getOldVersionsPath);
    if(onWin)
        path=path.replace(":\\","\\");
    return buildNormalizedPath(getOldVersionsPath~"/"~path);
}


string remOldVers(string path){
    if(!path.startsWith(addOldVers("")))
        throw new Exception("the file it's not in the versioning folder");
    string newPath=path[addOldVers("").length..$];
    return newPath;
}

string getInternalFolder(){
    return buildNormalizedPath(expandTilde(internalFolder));
}

string getOldVersionsPath(){
    return buildNormalizedPath(getInternalFolder()~"/"~oldVersions);
}

void writeWithDir(string file,string content){
    import std.file: write;
    mkdirRecurse(file.dirName);
    write(file,content);
}

unittest{assert(fillTimestamp("2019-09-15T12:24:43.581")=="2019-09-15T12:24:43.5810000");}
string fillTimestamp(string timestamp){
    if(timestamp.length>=27) return timestamp;
    string t1=timestamp;
    for(ulong i=timestamp.length;i<27;i++){
        t1~="0";
    }
    return t1;
}

public bool onWin(){
    import std.system:os,OS;
    import std.conv:to;
    switch(os){
    case OS.win32,OS.win64:
        return true;
    case OS.linux:
        return false;
    default:
        throw new Exception("Operating system not supported");
    }
}
//                 call,  alg,
Log log=Log.newLog(false,false);
