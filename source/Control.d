module Control;

import std.path;
import std.file;
import std.stdio: writeln, File, readln;
import std.datetime : SysTime;
import core.thread;
import std.algorithm.sorting: sort;
import std.datetime;
import std.format;
import std.algorithm;
import std.array:replace;
import std.string: indexOf;
import std.conv:to;
import Util; 

class Control{
    private SysTime[string] filesRecord;
    static private Control[string] controls; 
    public const string path;

    static Control createNew(string path){
        log.call("createNew",path);
        if(path in controls){
            log.alg("another control with the same path already exists");
            log.ret(controls[path]);
            return controls[path];
        }
        Control c=new Control(path);
        controls[path]=c;
        log.ret(c);
        return c;
    }
    private this(string path){
        log.call("new Control", path);
        if(!path.isAbsolute())
            throw new PathNotAbsoluteExc("the used path sould be absolute, the relative path are not supported");
        if(!exists(path))
            throw new FolderNotFoundExc("the folder "~path~" is not found");
        this.path=path;
        mkdirRecurse(path.addOldVers());
        loadStateFromVersionedFiles();
        scan();
        log.ret();
    }
    static void resetControls(){
        controls.clear();
    }

    void scan(){
        log.call("scan");
        Change[] files=getFileChangesAndLoadNewFiles();
        //copy the files
        foreach(mod;files){
            versionAFile(mod.fileName,mod.timestamp,mod.type);
        }
        log.ret();
    }

    void versionAFile(string file, string timestamp, Change.Type modType){
        log.call("versionAFile",file);
        string fileName=constructName(file,timestamp,modType);
        mkdirRecurse(fileName.addOldVers().dirName);
        if(modType!=Change.Type.DEL){
            log.alg("coping "~file~" "~fileName.addOldVers());
            copy(file,fileName.addOldVers());
            log.alg("copied");
        }else{
            write(fileName.addOldVers(),"");
        }
        log.ret();
    }

    static string constructName(string file,string timestamp,Change.Type modType){
        string fileName=file.dirName~"/"~modType~timestampFileFormat(timestamp)~fsep~file.baseName;
        return buildNormalizedPath(fileName);
    }

    static string timestampFileFormat(string timestamp){
        return timestamp.replace(":","_");
    }

    static string timestampISOFormat(string timestamp){
        return timestamp.replace("_",":");
    }
    
    Change[] getFileChangesAndLoadNewFiles(){
        log.call("getFileChangesAndLoadNewFiles");
        //current list of files
        DirEntry[] files=getAllFiles();
        SysTime[string] newFileList;//used to check the deleted files
        Change[] filesChanged;
        foreach(DirEntry file;files){
            log.alg("Scanning file:"~file.name~":"~file.timeLastModified.toISOExtString);
            if(file.name in filesRecord){
                if(filesRecord[file.name]!=file.timeLastModified){
                    Change c;
                    c.fileName=file.name;
                    c.timestamp=file.timeLastModified.toISOExtString();
                    c.type=Change.Type.MOD;
                    filesChanged ~= c;
                }
                //TODO use another list to list the deleted files and modify the filesRecord variable just at the end of the process
                filesRecord.remove(file.name);
            }else{
                Change c;
                c.fileName=file.name;
                c.timestamp=file.timeLastModified.toISOExtString();
                c.type=Change.Type.CRT;
                filesChanged ~= c;
            }
            newFileList[file.name]=file.timeLastModified;
        }
        log.alg("deleted:");
        log.algTab(filesRecord);
        foreach(string deletedFile; filesRecord.byKey){
            Change c;
            c.fileName=deletedFile;
            c.timestamp=Clock.currTime().toISOExtString();
            c.type=Change.Type.DEL;
            filesChanged ~= c;
        }
        filesRecord = newFileList;
        log.alg("filesChanged:");
        log.algTab(filesChanged);
        log.alg("filesRecord:");
        log.algTab(filesRecord);
        log.ret();
        return filesChanged;
    }

    private DirEntry[] getAllFiles(){
        DirEntry[] list=[];
        foreach(DirEntry el; dirEntries(path,SpanMode.depth)){
            if(el.isFile())
                list ~= el;
        }
        return list;
    }

    private void loadStateFromVersionedFiles(){
        log.call("loadStateFromVersionedFiles");
        SysTime[string] lastTimeFileVersion;
        foreach(DirEntry el; dirEntries(path.addOldVers(),SpanMode.depth)){
            if(el.isFile()){
                Change versInfo=extractChangeInfo(el.name.remOldVers(),"");
                if(versInfo.fileName in lastTimeFileVersion){
                    immutable string timestampInMap=lastTimeFileVersion[versInfo.fileName].toISOExtString();
                    if(versInfo.timestamp>timestampInMap)
                        lastTimeFileVersion[versInfo.fileName]=SysTime.fromISOExtString(versInfo.timestamp);
                }
                lastTimeFileVersion[versInfo.fileName]=SysTime.fromISOExtString(versInfo.timestamp);
            }
        }
        filesRecord=lastTimeFileVersion;
        log.alg(filesRecord);
        log.ret();
    }
}


struct Change{
    enum Type{CRT='C',MOD='M',DEL='D'};
    Type type;
    string text;
    string timestamp;
    string fileName;

    bool opEquals(const Change o) const{
        return this.fileName==o.fileName && this.timestamp==o.timestamp && this.type==o.type && this.text==o.text;
    }
    
    int opCmp(const Change o) const{
        //this<o ==-1
        //this=o ==0
        //this>o ==1
        if(this.fileName!=o.fileName){
            return this.fileName<o.fileName? -1:1;
        }
        else{
            if(this.timestamp!=o.timestamp)
                return this.timestamp<o.timestamp? -1:1;
            else{
                if(this.type!=o.type)
                    return this.type<o.type? -1:1;
                else{
                    if(this.type!=o.type)
                        return this.type<o.type? -1:1;
                    else
                        return 0;
                }
            }
        }
    }

    static Type getType(const char t){
        if(t=='C') return Type.CRT;
        if(t=='M') return Type.MOD;
        if(t=='D') return Type.DEL;
        throw new Exception("Type not recognized");
    }
}

class FolderNotFoundExc:Exception{
    this(string msg, string file = __FILE__, size_t line = __LINE__) {
        super(msg, file, line);
    }
}

Change extractChangeInfo(string fileVers,string text){
    log.call("extractChangeInfo","fileVers: "~fileVers);
    Change.Type type=Change.getType(fileVers.baseName[0]);
    string fileName=extractFileName(fileVers);
    log.alg("fileName:"~fileName);
    string timestamp=fileVers.baseName[1..$-(fileName.baseName.length+1)];
    timestamp=Control.timestampISOFormat(timestamp);//TODO use one version in all the system, map the toISOExtString function
    Change c;
    c.type=type;
    c.timestamp=timestamp;
    c.text=text;
    c.fileName=fileName;
    log.ret();
    return c;
}

unittest{assert(extractFileName("folder1/C2019-09-15T12:24:43.5810000"~fsep~"file.txt")=="folder1/file.txt");};
private string extractFileName(string fileVers){
    uint idx=to!uint(fileVers.baseName.indexOf(fsep));
    string fileName=fileVers.baseName[idx+1..$];
    return buildNormalizedPath(fileVers.dirName~"/"~fileName);
}

private bool isAVersionOf(string savedFile, string searchedFile){
    return isExpectedPath(savedFile,searchedFile)
        && isExpectedName(savedFile,searchedFile);
}

private bool isExpectedName(string savedFile, string searchedFile){
    return savedFile.baseName.endsWith(searchedFile.baseName);
}
    
private bool isExpectedPath(string savedFile, string searchedFile){
    return savedFile.dirName==searchedFile.addOldVers().dirName;
}

Change[] getFileVersions(string fileName){
    log.call("getFileVersions","fileName:"~fileName);
    string path=fileName.dirName();
    if(!path.isAbsolute())
        throw new PathNotAbsoluteExc("the used path should be absolute, the relative path are not supported");
    if(!exists(path.addOldVers()) || !path.addOldVers().isDir){
        throw new FolderNotExistsExc("the folder of the specified file doesn't seems to be versioned");
    }
    Change[] versions;
    //load all the files that ends with that name from the filesystem
    foreach(DirEntry file;dirEntries(path.addOldVers(),SpanMode.depth)){
        log.alg("file:"~file.name);
        if(file.isFile() && isAVersionOf(file.name,fileName)){
            log.algTab("the file is a version of the searched one");
            string text=cast(string)read(file.name());
            versions ~= extractChangeInfo(file.name().remOldVers(),text);
        }
    }
    sort(versions);
    log.ret(versions);
    return versions;
}

string[] getAllVersionedFiles(){
    log.call("getAllVersionedFiles");
    string[] files=[];
    foreach(DirEntry file;dirEntries("".addOldVers(),SpanMode.depth)){
        log.alg("file:"~file.name);
        if(file.isFile()){
            string name=extractFileName(file.name());
            name=name.remOldVers();
            name=name.reformatName();
            if(!files.canFind(name)){
                log.alg("file added:"~name);
                files ~= name;
            }
        }
    }
    log.ret(files);
    return files;
}

private string reformatName(string path){
    string newPath=path;
    if(onWin){
        if(!(newPath[0]=='\\' || newPath[0]=='/') || !(newPath[2]=='\\' || newPath[2]=='/'))//ensure that the path is in the right format, /C/folder
            throw new Exception("path specified not expected");
        newPath=newPath[1..$];
        newPath=newPath[0]~":"~newPath[1..$];
    }
    return buildNormalizedPath(newPath);
}

class FolderNotExistsExc:Exception{
    this(string msg, string file = __FILE__, size_t line = __LINE__) {
        super(msg, file, line);
    }
}

class PathNotAbsoluteExc:Exception{
    this(string msg, string file = __FILE__, size_t line = __LINE__) {
        super(msg, file, line);
    }
}

