module GeneralTest;

import std.path;
import std.file;
import std.stdio: writeln, File, readln;
import std.datetime : SysTime;
import core.thread;
import std.algorithm.sorting: sort;
import std.format;
import std.algorithm;
import std.exception: assertThrown;
import std.conv: to;
import Control;
import Logging:Log;
import Util;
import Settings;
import Scanning;


unittest{//test of the complete and basic functionality
    setupTest();
    immutable long freq = 10;
    addSetting(Setting.Key.SCAN,dirForTest);
    addSetting(Setting.Key.SCAN,otherDirForTest);
    addSetting(Setting.Key.FREQ,to!string(freq));
    Util.start();
    string file1=dirForTest~"/file1.txt";
    string file2=otherDirForTest~"/file2.txt";
    write(file1,"text");
    Thread.sleep((freq+5).msecs);
    write(file2,"different text");
    write(file1,"text a");
    Thread.sleep((freq+5).msecs);
    write(file1,"text b");
    Thread.sleep((freq+5).msecs);
    string[] changedFiles=getAllVersionedFiles();
    assert(changedFiles.length==2);
    assert(changedFiles.canFind(file1));
    assert(changedFiles.canFind(file2));
    foreach(string changedFile;changedFiles){
	if(changedFile==file1){
	    Change[] versions=getFileVersions(changedFile);
	    assert(versions.length==3);
	    assert(versions[0].text=="text");
	    assert(versions[1].text=="text a");
	    assert(versions[2].text=="text b");
	}else if(changedFile==file2){
	    Change[] versions=getFileVersions(changedFile);
	    assert(versions.length==1);
	    assert(versions[0].text=="different text");
	}
    }
    Util.stop();
}

unittest{//test for no settings at the start
    setupTest();
    Util.start();
    Util.stop();
}

unittest{//check exception for a not absolute path
    assertThrown!PathNotAbsoluteExc(getFileVersions("normalFolder/file1.txt"));
}


