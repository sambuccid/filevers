module ScanningTest;

import std.path;
import std.file;
import std.stdio: writeln, File, readln;
import std.datetime : SysTime;
import core.thread;
import std.algorithm.sorting: sort;
import std.datetime;
import std.format;
import std.algorithm;
import std.exception: assertThrown;
import Control;
import Logging:Log;
import Util;
import Scanning;

//--creation and basic functionality
/*unittest{
    setupTest();
    immutable int freq=100;
    Scanning.start(dirForTest,freq);
    string file=dirForTest~"/file1.txt";
    string file2=dirForTest~"/file2.txt";
    //write a file
    write(file,"text");
    //wait
    Thread.sleep((freq+5).msecs);
    //write another file
    write(file2,"different text");
    //modify the first file
    write(file,"text a");
    //wait
    Thread.sleep((freq+5).msecs);
    //modify the first file again
    write(file,"text b");
    //wait
    Thread.sleep((freq+5).msecs);
    Change[] versions1=getFileVersions(file);
    Change[] versions2=getFileVersions(file2);
    Scanning.stop();
    assert(versions1.length==3);
    assert(versions1[0].text=="text");
    assert(versions1[1].text=="text a");
    assert(versions1[2].text=="text b");
    assert(versions2.length==1);
    assert(versions2[0].text=="different text");
}*/
