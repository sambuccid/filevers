module SettingsTest;

import std.path;
import std.file;
import std.stdio: writeln, File, readln;
import std.datetime : SysTime;
import core.thread;
import std.algorithm.sorting: sort;
import std.datetime;
import std.format;
import std.conv: to;
import std.algorithm;
import std.exception: assertThrown;
import Control: Control,Change,FolderNotFoundExc, getAllVersionedFiles;
import Logging:Log;
import Util;
import Settings;


unittest{//saving setting
    setupTest();
    Setting[Setting.Key] sett=getSettings();
    assert(sett.length==0);
    addSetting(Setting.Key.SCAN,dirForTest);
    addSetting(Setting.Key.SCAN,otherDirForTest);
    sett=getSettings();
    assert(sett.length==1);
    assert(sett[Setting.Key.SCAN].getValueList().length==2);
    assert(sett[Setting.Key.SCAN].getValueList().canFind(dirForTest));
    assert(sett[Setting.Key.SCAN].getValueList().canFind(otherDirForTest));
    assert(exists(getPathFileSettings()));
}

unittest{//delete configurations
    setupTest();
    addSetting(Setting.Key.SCAN,dirForTest);
    addSetting(Setting.Key.SCAN,otherDirForTest);
    addSetting(Setting.Key.FREQ,"50");
    deleteSetting(Setting.Key.SCAN,otherDirForTest);
    Setting[Setting.Key] sett=getSettings();
    assert(sett.length==2);
    assert(sett[Setting.Key.SCAN].getValueList().length==1);
    assert(sett[Setting.Key.SCAN].getValueList()[0]==dirForTest);
    assert(sett[Setting.Key.FREQ].getValueList().length==1);
    assert(sett[Setting.Key.FREQ].getValueList()[0]=="50");
}

unittest{//delete not existing configuration
    setupTest();
    assert(deleteSetting(Setting.Key.SCAN,"property that not exists")==false);
}

unittest{//same values duplicated
    setupTest();
    addSetting(Setting.Key.SCAN,dirForTest);
    addSetting(Setting.Key.SCAN,dirForTest);
    Setting[Setting.Key] sett=getSettings();
    assert(sett.length==1);
    assert(sett[Setting.Key.SCAN].getValueList().length==1);
    assert(readText(getPathFileSettings()).count("\n")==1);
}

unittest{//syntactic validation of settings file, multiple delimiters
    setupTest();
    write(getPathFileSettings(),"scanFolder="~dirForTest~"\nscanFolder=oth=erTestDir\n");
    bool thrown=false;
    try{
        refreshSettings();
    }catch(SettingParseExc e){
        if(e.errType==SettingParseExc.ErrType.MultiDelimiter && e.lineNumber==1 && e.lineStr=="scanFolder=oth=erTestDir")
            thrown=true;
    }
    assert(thrown);
}

unittest{//syntactic validation of settings file, no delimiter
    setupTest();
    write(getPathFileSettings(),"scanFolder="~dirForTest~"\nscanFolderotherTestDir");
    bool thrown=false;
    try{
        refreshSettings();
    }catch(SettingParseExc e){
        if(e.errType==SettingParseExc.ErrType.NoDelimiter && e.lineNumber==1 && e.lineStr=="scanFolderotherTestDir")
            thrown=true;
    }
    assert(thrown);
}

unittest{//syntactic validation of settings file, empty value
    setupTest();
    write(getPathFileSettings(),"scanFolder=\n");
    bool thrown=false;
    try{
        refreshSettings();
    }catch(SettingParseExc e){
        if(e.errType==SettingParseExc.ErrType.EmptyValue && e.lineNumber==0 && e.lineStr=="scanFolder=")
            thrown=true;
    }
    assert(thrown);
}

unittest{//syntactic validation of settings file, blank value
    setupTest();
    write(getPathFileSettings(),"scanFolder=  \n");
    bool thrown=false;
    try{
    refreshSettings();
    }catch(SettingParseExc e){
        if(e.errType==SettingParseExc.ErrType.EmptyValue && e.lineNumber==0 && e.lineStr=="scanFolder=")
            thrown=true;
        else
            writeln("wrong paremeter in the exception:" ~ to!string(e.errType)~" "~to!string(e.lineNumber)~" "~e.lineStr);
    }
    assert(thrown);
}

unittest{//properties name validation of settings file
    setupTest();
    write(getPathFileSettings(),"scaFolder="~dirForTest~"\n");//note the missing n in the property name
    bool thrown=false;
    try{
        refreshSettings();
    }catch(SettingParseExc e){
        if(e.errType==SettingParseExc.ErrType.InvalidPropName && e.lineNumber==0 && e.lineStr=="scaFolder="~dirForTest)
            thrown=true;
    }
    assert(thrown);
}

unittest{//properties type of value validation of setting file
    setupTest();
    string prop="scanFrequency=fifteenoranothertextthatisnotanumber";
    write(getPathFileSettings(),prop~"\n");
    bool thrown=false;
    try{
        refreshSettings();
    }catch(SettingParseExc e){
        if(e.errType==SettingParseExc.ErrType.InvalidPropValue && e.lineNumber==0 && e.lineStr==prop)
            thrown=true;
    }
    assert(thrown);
}

unittest{//properties value validation of setting file, not existing folder
    setupTest();
    string prop="scanFolder=teDir";
    write(getPathFileSettings(),prop~"\n");
    bool thrown=false;
    try{
        refreshSettings();
    }catch(SettingParseExc e){
        if(e.errType==SettingParseExc.ErrType.InvalidPropValue && e.lineNumber==0 && e.lineStr==prop)
            thrown=true;
    }
    assert(thrown);
}

unittest{//number of same properties validation for properties that can appear just one time in the setting file
    setupTest();
    write(getPathFileSettings(),"scanFrequency=15\nscanFrequency=20\n");
    bool thrown=false;
    try{
        refreshSettings();
    }catch(SettingParseExc e){
        if(e.errType==SettingParseExc.ErrType.UnexpectedMultipleValues && e.lineNumber==1)
            thrown=true;
    }
    assert(thrown);
}

unittest{//invalid property value inserted
    setupTest();
    assertThrown!SettingParseExc(addSetting(Setting.Key.FREQ,"aasdll"));
}

unittest{//the property that causes an error when added, should be deleted from the settings file or not saved at all
    setupTest();
    addSetting(Setting.Key.SCAN,dirForTest);
    try{
        addSetting(Setting.Key.SCAN,"aa=ee");
    }catch(SettingParseExc e){
        //expected
    }
    assert(readText(getPathFileSettings()).count("\n")==1);
}

unittest{//the settings for the scan throws an error if the folder is not specified with an absolute path
    setupTest();
    bool thrown=false;
    try{
        addSetting(Setting.Key.SCAN,"testDir");
    }catch(SettingParseExc e){
        if(e.errType==SettingParseExc.ErrType.InvalidPropValue)
            thrown=true;
    }
    assert(thrown);
}

