module DifferencesTest;
import std.stdio: writeln;
import Util;
import Differences;

/* the format that define the differences is the seguent:
 * line1 modifier line2
 * line1 is the number of the line in the file 1
 * line 2 is the number of the line in the file 2
 * the modifier specifies if the line is in addition(>),is in "less"(<) or is just modifier(~) from the file 1 respect to the file 2
 * another way to easly understand it is looking at the arrow, if the arrow point to the right then the line in the first file "should be added" to the second file, if the arror is in the other direction then the first file is the file that miss the line
 * this means that for example if the file1 has 3 lines and the file2 has just on line, the probable result will be something like: [{1>1},{2>2}]
 * this one will be defined better during the usnit tests
 */


/* the line number 1 and the number 3 are similar in both files but actually different, so the return will be
 * 1~1
 * 3~3
 */
unittest{//basic functionality, line differences between text
    setupTest();
    string a="aa\nbb\ncc\ndd\n";
    string b="aa\nbb1\ncc\nd1d1\n";
    LineDifference[] lines=linesDifferences(a,b);
    assert(lines.length==2);
    assert(lines[0].lineFile1==1);
    assert(lines[0].lineFile2==1);
    assert(lines[0].modifier=='~');
    assert(lines[1].lineFile1==3);
    assert(lines[1].lineFile2==3);
    assert(lines[1].modifier=='~');
}

/* the last 2 lines are missing in the first file, the result will be:
 * 2<2
 * 2<3
 * is used 2 as the line in the first file in both cases because the index always refers to the actual file and not to the file that ideally would be created from the conjunction of the 2 files
 * this could be read as "both the lines are missing after the line number 1"
 */
unittest{//strings with different length
    setupTest();
    string a="aa\nbb\n";
    string b="aa\nbb\ncc\ndd\n";
    LineDifference[] lines=linesDifferences(a,b);
    assert(lines.length==2);
    assert(lines[0].stringify=="2<2");
    assert(lines[1].stringify=="2<3");
}

/* the first file has the line a,c,d,e,f meanwhile the second file has the lines a,b,c,d,e
 * also the line d of the first file is a bit modified
 * so the expected result would be:
 * 1<1 (line b missing in the first file)
 * 2~3 (line d a bit different in the 2 files)
 * 3>4 (line e missing in the second file)
 */
unittest{//string that have some lines added and removed in both the files
    setupTest();
    string a="aa\ncc\nd1\nee\nff\n";
    string b="aa\nbb\ncc\ndd\nff\n";
    LineDifference[] lines=linesDifferences(a,b);
    assert(lines.length==3);
    assert(lines[0].stringify=="1<1");
    assert(lines[1].stringify=="2~3");
    assert(lines[2].stringify=="3>4");
}



