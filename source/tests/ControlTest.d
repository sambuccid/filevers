module ControlTest;

import std.path;
import std.file;
import std.stdio: writeln, File, readln;
import std.datetime : SysTime;
import core.thread;
import std.algorithm.sorting: sort;
import std.datetime;
import std.format;
import std.algorithm;
import std.exception: assertThrown;
import Control: Control,Change,FolderNotFoundExc,PathNotAbsoluteExc,FolderNotExistsExc,getAllVersionedFiles,getFileVersions;
import Logging:Log;
import Util;
import Settings;

//---getFileChangesAndLoadNewFiles---
unittest{//main functioning
    setupTest();
    File file1=File(dirForTest~"/file1.txt","w"); file1.writeln("test"); file1.close();
    write(dirForTest~"/file2.txt","aaa");

    Control control=Control.createNew(dirForTest);
    Thread.sleep(5.msecs);
    
    file1.open(dirForTest~"/file1.txt","a"); file1.write("test2"); file1.close();
    
    Change[] cambiati=control.getFileChangesAndLoadNewFiles();
    assert(cambiati.length==1);
    assert(cambiati[0].fileName==dirForTest~"/file1.txt");
}

unittest{//file with same name
    setupTest();

    write(dirForTest~"/file2.txt","aaa");
    writeWithDir(dirForTest~"/folder1/file2.txt","aaabbb");
    
    Control control=Control.createNew(dirForTest);
    Thread.sleep(5.msecs);

    write(dirForTest~"/folder1/file2.txt","cccccdddddd");
    
    Change[] cambiati=control.getFileChangesAndLoadNewFiles();
    assert(cambiati.length==1);
    assert(cambiati[0].fileName==dirForTest~"/folder1/file2.txt");

}

unittest{//file with same name in control class
    setupTest();
    
    string filePt=dirForTest~"/file.txt";
    write(filePt,"nice text");
    
    Control control=Control.createNew(dirForTest);

    Thread.sleep(5.msecs);
    string fileStessoNome=dirForTest~"/a/file.txt";
    writeWithDir(fileStessoNome,"another nice text, in a different file");

    control.scan();

    Change[] versioniPrimoFile=getFileVersions(filePt);
    assert(versioniPrimoFile.length==1);
    assert(versioniPrimoFile[0].text=="nice text");

    Change[] versioniSecondoFile=getFileVersions(fileStessoNome);
    assert(versioniSecondoFile.length==1);
    assert(versioniSecondoFile[0].text=="another nice text, in a different file");
}


unittest{//more version
    setupTest();

    string filePt=dirForTest~"/file.txt";
    write(filePt,"a text");

    Control control=Control.createNew(dirForTest);

    Thread.sleep(5.msecs);
    write(filePt,"another text");

    control.scan();

    Thread.sleep(5.msecs);
    write(filePt,"nother text");
    control.scan();

    Change[] versioni=getFileVersions(filePt);
    
    assert(versioni.length==3);
    assert(versioni[0].text=="a text");
    assert(versioni[1].text=="another text");
    assert(versioni[2].text=="nother text");    
}


unittest{//versions of a file
    setupTest();

    string fileName=dirForTest~"/a file.txt";
    write(fileName,"a file");
    //scannerizzo
    Control control=Control.createNew(dirForTest);
    //aspetto
    Thread.sleep(5.msecs);
    //modifico il file
    write(fileName," that was changing its text");
    //scannerizzo
    control.scan();
    //aspetto
    Thread.sleep(5.msecs);
    //modifico il file di nuovo
    write(fileName," knowing that just having all the version of it would make a complete sentence");
    //scannerizzo
    control.scan();
    //chiamo funzione per avere le versioni
    Change[] versioni=getFileVersions(fileName);
    //control che ci siano le 3 versioni
    assert(versioni.length==3);
    assert(versioni[0].text=="a file");
    assert(versioni[1].text==" that was changing its text");
    assert(versioni[2].text==" knowing that just having all the version of it would make a complete sentence");
}

unittest{//type of change
    setupTest();
    
    string file=dirForTest~"/a file.txt";
    write(file,"aaaa text");
    
    Control c=Control.createNew(dirForTest);

    Thread.sleep(5.msecs);
    remove(file);
    c.scan();

    Thread.sleep(50.msecs);
    write(file,"aaaaanother text");
    c.scan();

    Thread.sleep(5.msecs);
    write(file,"another text again");
    c.scan();
    
    //control le 3 versioni del file
    Change[] versioni=getFileVersions(file);
    assert(versioni.length==4);
    assert(versioni[0].type==Change.Type.CRT);
    assert(versioni[0].text=="aaaa text");
    assert(versioni[1].type==Change.Type.DEL);
    assert(versioni[1].text is null);
    assert(versioni[2].type==Change.Type.CRT);
    assert(versioni[2].text=="aaaaanother text");
    assert(versioni[3].type==Change.Type.MOD);
}

/*
unittest{//test of the timestamp
    setupTest();
   
    string file=dirForTest~"/file.tiicsti";
    write(file,"aaa");
    string timestamp=Clock.currTime().toISOExtString();//timestamp at the creation of the file
    Thread.sleep(200.msecs);
    Control c=Control.createNew(dirForTest);
   
    write(file,"aaab");
    string timestamp1=Clock.currTime().toISOExtString();//timestamp at the modification of the file
    Thread.sleep(200.msecs);
    c.scan();

    remove(file);
    Thread.sleep(200.msecs);
    
    string timestamp2=Clock.currTime().toISOExtString();//timestamp at the scan next to the deletion of the file
    c.scan();
    
    Change[] versioni=getFileVersions(file);
    assert(versioni.length==3);
    assert(versioni[0].timestamp.startsWith(timestamp[0..21]));
    assert(versioni[1].timestamp.startsWith(timestamp1[0..21]));
    assert(versioni[2].timestamp.startsWith(timestamp2[0..21]));
}*/

unittest{//all the versioned files
    setupTest();
    Control c=Control.createNew(dirForTest);
    string file1=dirForTest~"/file1.txt";
    write(file1,"ciaooooooooooooooo :)");
    string file2=dirForTest~"/file2.txt";
    write(file2,"beelllaaaaaaaaaaaaaaaaaaaaa");
    c.scan();
    Thread.sleep(5.msecs);
    write(file1,"aaoooaaooaa");
    c.scan();
    string[] changedFiles=getAllVersionedFiles();
    assert(changedFiles.length==2);
    assert(changedFiles.canFind(file1));
    assert(changedFiles.canFind(file2));
}

unittest{//all versioned files with also delete files
    setupTest();
    Control c=Control.createNew(dirForTest);
    string file1=dirForTest~"/file1.txt";
    write(file1,"beeellllllaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    c.scan();
    Thread.sleep(5.msecs);
    remove(file1);
    string file2=dirForTest~"/file2.txt";
    write(file2,"wazzzaaaaaaaa");
    c.scan();
    string[] changedFiles=getAllVersionedFiles();
    assert(changedFiles.length==2);
    assert(changedFiles.canFind(file1));
    assert(changedFiles.canFind(file2));
}

unittest{//file versioned also with different control
    setupTest();
    Control c=Control.createNew(dirForTest);
    Control c2=Control.createNew(otherDirForTest);
    string file1=dirForTest~"/file1.txt";
    write(file1,"beeellllllaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    string file2=otherDirForTest~"/file2.txt";
    write(file2,"wazzzaaaaaaaaa");
    c.scan();
    c2.scan();
    string[] changedFiles=getAllVersionedFiles();
    assert(changedFiles.length==2);
    assert(changedFiles.canFind(file1));
    assert(changedFiles.canFind(file2));
}

unittest{//exceptions
    setupTest();
    assertThrown!FolderNotFoundExc(Control.createNew(dirForTest~"1"));
}

unittest{//ripresa dell'ultima scansione
    setupTest();
    string file=dirForTest~"/file1.txt";
    write(file,"aaa");
    Control c=Control.createNew(dirForTest);
    Thread.sleep(5.msecs);
    write(file,"aaa plus bbb");
    c.scan();
    c=null;
    c=Control.createNew(dirForTest);
    Thread.sleep(5.msecs);
    write(file,"aaa plus bbb and also ccc");
    c.scan();
    Change[] versions=getFileVersions(file);
    assert(versions.length==3);
}

unittest{//check creation of control, not 2 control with the same path
    setupTest();
    Control c=Control.createNew(dirForTest);
    Control c1=Control.createNew(dirForTest);
    assert(c==c1);
}

unittest{//check exception for a not absolute path
    assertThrown!PathNotAbsoluteExc(Control.createNew("testDir"));
}

unittest{//check exception for a folder that it's not been versioned
    assertThrown!FolderNotExistsExc(getFileVersions(dirForTest~"/something/file1.txt"));
}



